class ControlPanel {
private:
  unsigned switchPort;
  unsigned greenPort;
  unsigned long switchPrevMillis = 0;
  bool battleMode = false;
  static const unsigned readDebounceTime = 200;
  bool canReadSwitch();
  bool isBattleOn();
  void disableMainLed();
  void enableMainLed();
public:
  ~ControlPanel() {};
  ControlPanel(unsigned switchPort, unsigned greenPort) {
    this->switchPort = switchPort;
    this->greenPort = greenPort;
  }
  void setup();
  bool checkBattleState();
};

bool ControlPanel::canReadSwitch() {
  return switchPrevMillis == 0;
}

void ControlPanel::setup() {
  pinMode(greenPort, OUTPUT);
  pinMode(switchPort, INPUT);
}

bool ControlPanel::checkBattleState() {
  bool state = isBattleOn();
  if (state) {
    disableMainLed();
  } else {
    enableMainLed();
  }
  return state;
}

bool ControlPanel::isBattleOn() {
  if (!canReadSwitch()) {
    unsigned long current = millis();
    if (current - switchPrevMillis >= readDebounceTime) {
      switchPrevMillis = 0;
    }
  }

  if (canReadSwitch() && digitalRead(switchPort) == HIGH) {
    battleMode = !battleMode;
    switchPrevMillis = millis();
  }
  return battleMode;
}

void ControlPanel::enableMainLed() {
    digitalWrite(greenPort, HIGH);
}

void ControlPanel::disableMainLed() {
    digitalWrite(greenPort, LOW);
}