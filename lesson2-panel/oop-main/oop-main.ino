#include "controlPanel.h"
#include "battleStation.h"


ControlPanel controlPanel = ControlPanel(2, 3);
BattleStation battleStation = BattleStation(4, 5);
void setup() {
  controlPanel.setup();
  battleStation.setup();
}

void loop() {
  if (controlPanel.checkBattleState()) {
    battleStation.battleOnLoop();
  } else {
    battleStation.battleOffLoop();
  }
}
