class BattleStation {
private:
  unsigned red1Port;
  unsigned red2Port;
  unsigned red1State = LOW;
  unsigned long previousToggleTime = 0;
  static const unsigned ledToggleTime = 250;

  bool isToggleTimeReached();
  void updateLeds();
public:
  ~BattleStation(){};
  BattleStation(unsigned red1Port, unsigned red2Port) {
    this->red1Port = red1Port;
    this->red2Port = red2Port;
  }
  void setup();
  void battleOnLoop();
  void battleOffLoop();
};

void BattleStation::setup() {
  pinMode(red1Port, OUTPUT);
  pinMode(red2Port, OUTPUT);
}

void BattleStation::battleOnLoop() {
  if (isToggleTimeReached()) {
    updateLeds();
  }
}

void BattleStation::battleOffLoop() {
  digitalWrite(red1Port, LOW);
  digitalWrite(red2Port, LOW);
}

bool BattleStation::isToggleTimeReached() {
  unsigned long current = millis();
  if (current - previousToggleTime >= ledToggleTime) {
    previousToggleTime = current;
    return true;
  }
  return false;
}

void BattleStation::updateLeds() {
  red1State = !red1State;
  digitalWrite(red1Port, red1State);
  digitalWrite(red2Port, !red1State);
}