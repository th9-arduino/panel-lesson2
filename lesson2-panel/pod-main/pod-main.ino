// constants
const unsigned LED_TOGGLE_DELAY = 250;
const unsigned SWITCH_DEBOUNCE = 200;

const int SWITCH_PORT = 2;
const int GREEN_PORT = 3;
const int RED1_PORT = 4;
const int RED2_PORT = 5;

// variables
unsigned long togglePrevMillis = 0;
unsigned long switchPrevMillis = 0;
bool battleMode = false;
int red1State = LOW;

void setup() {
  pinMode(GREEN_PORT, OUTPUT);
  pinMode(RED1_PORT, OUTPUT);
  pinMode(RED2_PORT, OUTPUT);
  pinMode(SWITCH_PORT, INPUT);
}

void loop() {
  if (isBattleOn()) {
    digitalWrite(GREEN_PORT, LOW);
    if (isToggleTimeReached()) {
      updateRedLeds();
    }
  } else {
    digitalWrite(GREEN_PORT, HIGH);
    digitalWrite(RED1_PORT, LOW);
    digitalWrite(RED2_PORT, LOW);
  }
}

bool isBattleOn() {
  if (!canReadSwitchInput()) {
    unsigned long current = millis();
    if (current - switchPrevMillis >= SWITCH_DEBOUNCE) {
      switchPrevMillis = 0;
    }
  }

  if (canReadSwitchInput() && digitalRead(SWITCH_PORT) == HIGH) {
    battleMode = !battleMode;
    switchPrevMillis = millis();
  }
  return battleMode;
}

bool canReadSwitchInput() {
  return switchPrevMillis == 0;
}

bool isToggleTimeReached() {
  unsigned long current = millis();
  if (current - togglePrevMillis >= LED_TOGGLE_DELAY) {
    togglePrevMillis = current;
    return true;
  }
  return false;
}

void updateRedLeds() {
  red1State = !red1State;
  digitalWrite(RED1_PORT, red1State);
  digitalWrite(RED2_PORT, !red1State);
}
