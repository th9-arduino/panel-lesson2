const unsigned TEMPERATURE_ANALOG_PIN = A0;
const unsigned LED_PIN_START = 3;
const unsigned LED_PIN_END = 5;

float referenceTemperature = 0.0;

float readTemperature() {
  int sensorValue = analogRead(TEMPERATURE_ANALOG_PIN);
  float voltage = sensorValue * (5000.0 / 1023.0);
  return (voltage - 500) / 10;
}

float getAverageTemperature(int probesCount = 10, int measurementDelay = 500) {
  float temperatureSum = 0.0;
  for (int i = 0; i < probesCount; ++i) {
    temperatureSum += readTemperature();
    delay(measurementDelay);
  }
  return temperatureSum / probesCount;
}

void setup() {
  Serial.begin(9600);
  for (int i = LED_PIN_START; i <= LED_PIN_END; ++i) {
    pinMode(i, OUTPUT);
  }

  digitalWrite(LED_PIN_START, HIGH);
  referenceTemperature = getAverageTemperature();
  digitalWrite(LED_PIN_START, LOW);
}

void controlLed(float temperature) {
  int step = 2;
  for (int i = 1; i <= LED_PIN_END - LED_PIN_START + 1; ++i) {
    float threshold = referenceTemperature + (step * i);
    digitalWrite(i + LED_PIN_START - 1, temperature >= threshold ? HIGH : LOW);
  }
}

void loop() {
  float temperature = readTemperature();
  controlLed(temperature);

  Serial.print("Reference: ");
  Serial.print(referenceTemperature);
  Serial.print(", Actual: ");
  Serial.println(temperature);
  delay(1000);
}